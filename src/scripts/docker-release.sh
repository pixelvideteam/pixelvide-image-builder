#!/usr/bin/env bash
#
# Release to Bintray.
#
# On master, we push both a :latest tag, and :<next-version>
# On release/<foo> branches, we only push :<next-version>
#
# Required globals:
#   AWS_DEFAULT_REGION
#   BITBUCKET_BRANCH
#   BITBUCKET_BUILD_NUMBER
#
# Optional globals:
#   AWS_ROLE_ARN
#
# Arguments:
#   REPOSITORY: The AWS ECR private repository we are pushing to.
#
set -ex

REPOSITORY=$1
VERSION=${VERSION:-$(semversioner current-version)}
AWS_ROLE_ARN=${AWS_ROLE_ARN:=""}

if [ ! -z "$AWS_ROLE_ARN" ]
then
  temp_role=$(aws sts assume-role \
     --role-arn $AWS_ROLE_ARN \
     --role-session-name 'pixelvide-image-builder')

  export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq -r .Credentials.AccessKeyId)
  export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq -r .Credentials.SecretAccessKey)
  export AWS_SESSION_TOKEN=$(echo $temp_role | jq -r .Credentials.SessionToken)
fi

# Login to docker registry on AWS
docker_login() {
    aws ecr get-login-password | docker login --username AWS --password-stdin $DOCKER_ECR_REPO_URL
}

# Generate all tags.
generate_tags() {
    if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
        tags=("latest" "${BITBUCKET_BRANCH}" "${BITBUCKET_COMMIT}" "${VERSION}")
    else
        tags=("${BITBUCKET_BRANCH}" "${BITBUCKET_COMMIT}" "${VERSION}")
    fi
}

# Build and tag with an intermediate tag.
docker_build() {
    docker build -t ${REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER} .
}

# Tag with final tags and push.
docker_push() {
    for tag in "${tags[@]}"; do
        docker tag "${REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${REPOSITORY}:${tag}"
        docker push "${REPOSITORY}:${tag}"
    done
}

docker_login
generate_tags
docker_build
docker_push
