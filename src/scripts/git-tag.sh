#!/usr/bin/env bash
#
# tag $version and push
#

version=$1

git add pipe.yml

echo "Tagging for release ${version}"
git commit -m "Tagging for release ${version}"
git tag -a -m "Tagging for release ${version}" "${version}"
git push origin "${version}"
