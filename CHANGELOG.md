# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.5

- patch: Example added for cross acount push

## 0.2.4

- patch: git added in base image

## 0.2.3

- patch: JQ added in build image

## 0.2.2

- patch: update version default value fixed

## 0.2.1

- patch: role session name updated to image-builder

## 0.2.0

- minor: cross account image push feature added

## 0.1.0

- minor: Initial Release
